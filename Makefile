server:
	go run cmd/main.go

proto_url:
	protoc --go_out=. --go_opt=paths=source_relative \
    	   --go-grpc_out=. --go-grpc_opt=paths=source_relative pkg/pb/url.proto

