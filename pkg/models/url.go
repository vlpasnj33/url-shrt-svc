package models

type Url struct {
	Id 			 int64  `json:"id" gorm:"primaryKey"`
	OriginalUrl	 string	`json:"original_url"`
	ShortenUrl   string	`json:"shorten_url"`
	// UserId		 int64	`json:"user_id"`
}