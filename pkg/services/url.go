package services

import (
	"context"
	"go-grpc-url-shrt-svc/pkg/db"
	"go-grpc-url-shrt-svc/pkg/models"
	pb "go-grpc-url-shrt-svc/pkg/pb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	H db.Handler
}

func (s *Server) CreateUrl(ctx context.Context, req *pb.CreateUrlRequest) (*pb.CreateUrlResponse, error) {
	var url models.Url

	url = models.Url{
		OriginalUrl: req.OriginalUrl,
		ShortenUrl: req.ShortenUrl,
		// UserId: req.UserId,
	}

	if result := s.H.DB.Create(&url); result.Error != nil {
		return nil, status.Error(codes.Internal, "Failed to create URL")
	}

	return &pb.CreateUrlResponse{
		ShortenUrl: url.ShortenUrl,
	}, nil
}